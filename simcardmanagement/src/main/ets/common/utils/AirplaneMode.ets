/**
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Message/AirplaneMode utils
 */
const SETTING_AIRPLANE_MODE_URI = "datashare:///com.ohos.settingsdata/entry/settingsdata/SETTINGSDATA?Proxy=true";
const QUERY_AIRPLANE_MODE_KEY = "airplane_mode";
const TAG = 'AirplaneMode';

import HiLog from './LogUtils';
import dataShare from '@ohos.data.dataShare'
import dataSharePredicates from '@ohos.data.dataSharePredicates';

let dataShareHelper;

/**
 * init DataShareHelper.
 */
export async function initDataShareHelper(callback?) {
  try {
    dataShare.createDataShareHelper(globalThis.simCardAbilityContext, SETTING_AIRPLANE_MODE_URI, (err, data) => {
      if (err != undefined) {
        HiLog.e(TAG, 'initDataShareHelper error: code: ' + err.code + ', message: ' + err.message);
        return;
      }
      dataShareHelper = data;
      if (callback) {
        callback(data)
      }
    });
  } catch (err) {
    HiLog.e(TAG, 'initDataShareHelper error: code: ' + err.code + ', message: ' + err.message);
  }
}

/**
 * add AirPlaneMode Listener.
 */
export async function addAirPlaneModeListener(callback) {
  HiLog.i(TAG, 'addAirPlaneModeListener');
  try {
    dataShare.createDataShareHelper(globalThis.simCardAbilityContext, SETTING_AIRPLANE_MODE_URI, (err, data) => {
      if (err != undefined) {
        HiLog.e(TAG, 'addAirPlaneModeListener error: code: ' + err.code + ', message: ' + err.message);
        return;
      }
      dataShareHelper = data;
      try {
        dataShareHelper.on("dataChange", SETTING_AIRPLANE_MODE_URI, () => {
          HiLog.i(TAG, "addAirPlaneModeListener dataChange");
          queryAirPlaneMode(callback);
        });
        queryAirPlaneMode(callback);
      } catch (err) {
        HiLog.e(TAG, 'addAirPlaneModeListener error: code: ' + err.code + ', message: ' + err.message);
      }
    });
  } catch (err) {
    HiLog.e(TAG, 'addAirPlaneModeListener error: code: ' + err.code + ', message: ' + err.message);
  }
}

/**
 * remove AirPlaneMode Listener.
 */
export async function removeAirPlaneModeListener() {
  HiLog.i(TAG, 'removeAirPlaneModeListener');
  if (!dataShareHelper) {
    HiLog.e(TAG, "removeAirPlaneModeListener dataShareHelper null");
    return;
  }
  try {
    dataShareHelper.off("dataChange", SETTING_AIRPLANE_MODE_URI, () => {
      HiLog.i(TAG, "removeAirPlaneModeListener dataChange");
    });
  } catch (err) {
    HiLog.e(TAG, 'removeAirPlaneModeListener error: code: ' + err.code + ', message: ' + err.message);
  }
}

/**
 * query AirPlaneMode.
 */
export async function queryAirPlaneMode(callback) {
  HiLog.i(TAG, 'queryAirPlaneMode ');
  if (!dataShareHelper) {
    HiLog.e(TAG, "queryAirPlaneMode dataShareHelper null");
    return;
  }
  let condition = new dataSharePredicates.DataSharePredicates();
  try {
    // ID, KEYWORD, VALUE
    dataShareHelper.query(SETTING_AIRPLANE_MODE_URI, condition, null).then((data) => {
      HiLog.i(TAG, 'queryAirPlaneMode query succeed');
      let hasNext = data.goToFirstRow();
      while (hasNext) {
        if (data.getString(data.getColumnIndex("KEYWORD")) == QUERY_AIRPLANE_MODE_KEY) {
          HiLog.i(TAG, 'queryAirPlaneMode query succeed return key');
          callback(data.getLong(data.getColumnIndex("VALUE")));
          return;
        }
        hasNext = data.goToNextRow();
      }
      callback(-1);
    }).catch((err) => {
      HiLog.e(TAG, 'queryAirPlaneMode query in error: err: ' + JSON.stringify(err));
    });
  } catch (err) {
    HiLog.e(TAG, 'queryAirPlaneMode query out error: code: ' + err.code + ', message: ' + err.message);
  }
}

